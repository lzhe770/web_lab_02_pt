A demonstration of merge conflicts
----------------------------------

Describe conflicts here

```
An equation:
 
a = 5
b = 7
 
a * b = 45
```

Solving merge conflicts
-----------------------

1. Try to let git solve the conflict automatically
2. If that failed, open the conflicting file and locate the conflict markers
3. Determine if I have to pick one version, or if I want parts from both
4. Action the change
5. Commit the changes
6. Done!

How do we feel about merging
----------------------------

Merging is great because most of the time git can resolve merges for me. 
Sometimes I have to deal with conflicts but it isn't too bad